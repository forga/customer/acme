ACME
====

Ce [paquet Python](https://docs.python.org/fr/3/tutorial/modules.html#packages) fait parti des [projets client](https://gitlab.com/forga/customer/) du groupe [`forga`](https://gitlab.com/forga/). Pour plus d'informations sur ce groupe, visitez l'URL :

https://forga.gitlab.io/fr/


💡 C'est quoi l'idée ?
---------------------

Ce projet est une démonstration basique d'[empaquetage pour projet Django](https://docs.djangoproject.com/fr/3.1/intro/reusable-apps/), parce que la vie est trop courte pour [recopier du code](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas) !


📦 Qu'est ce qu'il y a dedans ?
-------------------------------

[`core`](https://gitlab.com/forga/tool/django/core/) et [`one-to-one`](https://gitlab.com/forga/tool/django/one-to-one/) : des _app_ provenant des [outils pour Django](https://gitlab.com/forga/tool/django/) du groupe [`forga`](https://gitlab.com/forga/).

```python
$ git remote -v
core	git@gitlab.com:forga/tool/django/core.git (fetch)
core	git@gitlab.com:forga/tool/django/core.git (push)
origin	git@gitlab.com:forga/customer/acme.git (fetch)
origin	git@gitlab.com:forga/customer/acme.git (push)

$ find . -maxdepth 1 -type d
.
./one_to_one
./.git
./core

$ cat requirements.txt
asgiref==3.2.10
Django==3.1
pytz==2020.1
sqlparse==0.3.1
git+https://gitlab.com/forga/tool/django/one_to_one.git@stable#egg=one_to_one
```


### Étapes de création de ce projet :


#### 1. Ajouter [`core`](https://gitlab.com/forga/tool/django/core/)

```shell
git clone git@gitlab.com:forga/tool/django/core.git acme
cd acme
git remote rename origin core
```
Ajouter le dépôt distant (_remote_) du projet `acme` :
```shell
git remote add origin git@gitlab.com:forga/customer/acme.git
```


#### 2. Ajouter [`one-to-one`](https://gitlab.com/forga/tool/django/one-to-one/)

Installer les dépendances dans un environnement virtuel :
```shell
path/to/python3.7 -m venv ~/.venvs/acme
source ~/.venvs/acme/bin/activate
pip install -r requirements.txt
```


#### 3. Et voilà !

```shell
./manage.py migrate
./manage.py runserver
```


❓ Mais pourquoi ?
-----------------

Cette organisation du projet vise à mutualiser le développement :

- `core` peut être utilisé dans plusieurs projets clients et recevoir autant de contributions (mise à jour, fonctionnalité, etc.) que l'on récupère avec : `git checkout core && git pull core stable:core`
- à l'inverse si on ajoute un contribution à `core` dans ce projet, on pourra les partager avec les autres projets utilisant cette base avec : `git push core core:mon_patch`
- `one-to-one` est développée [en dehors du projet](https://gitlab.com/forga/tool/django/one_to_one) et donc utilisable telle quelle dans d'autres projets.


🤝 Envie de contribuer ?
------------------------

Avec plaisir !

Il y a quelques conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) présentées dans [le manuel](https://forga.gitlab.io/process/fr/manuel/) ainsi qu'un [code de conduite](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/).

Et pourquoi ne pas proposer une [édition de cette page](https://gitlab.com/forga/customer/acme/-/edit/documentation/README-fr.md)?

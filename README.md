ACME
====

[ [🇫🇷 French version](README-fr.md) ]

This [Python package](https://docs.python.org/en/3/tutorial/modules.html#packages) is part of the [client projects](https://gitlab.com/forga/customer/) of the [`forga`](https://gitlab.com/forga/) group. For more information about this group, visit this URL :

https://forga.gitlab.io/en/


💡 What's the idea ?
-------------------

This project is a basic demonstration of [packaging for Django project](https://docs.djangoproject.com/en/3.1/intro/reusable-apps/), because life is too short to [copy code](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)!


📦 What's in it ?
----------------

[`core`](https://gitlab.com/forga/tool/django/core/) and [`one-to-one`](https://gitlab.com/forga/tool/django/one-to-one/): _app_ from the [Django tools](https://gitlab.com/forga/tool/django/) of the [`forga`](https://gitlab.com/forga/) group.

```python
git remote -v
core git@gitlab.com:forga/tool/django/core.git (fetch)
core git@gitlab.com:forga/tool/django/core.git (push)
origin git@gitlab.com:forga/customer/acme.git (fetch)
origin git@gitlab.com:forga/customer/acme.git (push)

find . -maxdepth 1 -type of
.
./one_to_one
./.git
/core

cat requirements.txt
asgiref==3.2.10
Django==3.1
pytz==2020.1
sqlparse==0.3.1
git+https://gitlab.com/forga/tool/django/one_to_one.git@stable#egg=one_to_one
```


### Steps to create this project :


#### 1. Add [`core`](https://gitlab.com/forga/tool/django/core/)

```shell
git clone git@gitlab.com:forga/tool/django/core.git acme
cd acme
git remote rename origin core
```
Add the remote repository (_remote_) of the `acme` project:
```shell
git remote add origin git@gitlab.com:forga/customer/acme.git
```


#### 2. Add [`one-to-one`](https://gitlab.com/forga/tool/django/one-to-one/)

Dependencies are installed in a virtual environment :
```shell
path/to/python3.7 -m venv ~/.venvs/acme
source ~/.venvs/acme/bin/activate
pip install -r requirements.txt
```


#### 3. Here we go !

```shell
./manage.py migrate
./manage.py runserver
```


❓ But why ?
------------

This organization of the project aims to mutualize the development :

- `core` can be used in several customer projects and receive as many contributions (update, functionality, etc.) as you get with : `git checkout core && git pull stable core:core`.
- conversely if we add a contribution to `core` in this project, we will be able to share them with other projects using this database with : `git push core:my_patch`.
- `one-to-one` is developed [outside the project](https://gitlab.com/forga/tool/django/one_to_one) and therefore usable as is in other projects.


🤝 Want to contribute ?
----------------------

With pleasure!

There are some conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) presented in [the manual](https://forga.gitlab.io/process/fr/manuel/) and a [code of conduct](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/).

And why not propose an [edition of this page](https://gitlab.com/forga/customer/acme/-/edit/documentation/README.md)?

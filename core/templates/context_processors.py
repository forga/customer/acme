"""
A request processors that return dictionaries to be merged into a
template context. Takes the request object as its only parameter
and returns a dictionary to add to the context.

These are referenced from the 'context_processors' option of the configuration
of a DjangoTemplates backend and used by RequestContext.

Inspiration from `django.template.context_processors`
"""
from django.conf import settings


def not_production(request):
    """
    Return context variables if in debug mode
    """
    context_extras = {}
    if settings.ENVIRONMENT != "production":
        context_extras["not_production"] = True

    return context_extras
